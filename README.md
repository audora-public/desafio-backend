# Desafio Backend

Olá, que bom que você chegou até aqui no nosso processo. Nessa fase, você irá mostrar um pouco dos seus conhecimentos técnicos. Você está recebendo agora uma proposta de um sistema simples que deverá ser implementado seguindo as definições abaixo.

Você deverá utilizar a linguagem Java e, preferencialmente, o framework Spring (Spring Boot). Em relação ao banco de dados escolhido, fica a seu critério, contanto que os passos para reproduzir o teste estejam claros na documentação do seu projeto.

Importante: quaisquer dúvidas relacionadas ao presente teste devem ser enviadas como resposta a esse e-mail, e a resposta será dada o mais breve possível.

###### Contextualização do Desafio

Você será responsável por desenvolver a API de um pequeno sistema de compras on-line. Nesse sistema, um usuário **administrador** do sistema deverá poder cadastrar produtos, categorias de produtos e descontos (promoções).

Um **cliente** poderá cadastrar-se no sistema com seu e-mail e senha e poderá adicionar produtos ao seu carrinho de compras.

Sempre que o cliente adicionar um produto ao seu carrinho, o sistema deverá aplicar todos os descontos disponíveis para os produtos do carrinho, bem como seu valor atualizado.

Todos os descontos aplicados devem ser listados no carrinho.

Os descontos podem ser de dois tipos: **percentual** ou um **valor fixo**. Os descontos podem ser aplicados por valor total do carrinho ou por categoria do produto.

*Bônus*: implementar descontos cumulativos e não cumulativos. Quando os descontos forem não cumulativos, o sistema deverá aplicar aquele que resulte no menor valor total da compra.

*Bônus*: adicionar formas de pagamento e descontos baseados nessas formas de pagamento. Caso o cliente escolha pagamento à vista, pode ser dado 10% de desconto em cima do valor total da compra.

*Bônus*: implementar cupons de desconto. O cliente pode escolher digitar o código de um cupom que adiciona um desconto ao seu carrinho.

###### Sobre a entrega

Ao concluir, você deverá disponibilizar publicamente seu código em algum repositório de sua preferência e responder esse e-mail com o respectivo link. No repositório deverá conter um guia para execução dos testes e para utilização das APIs.

Critérios de Avaliação

Tudo que você implementar será considerado. Porém, atente para os seguintes pontos de avaliação:
- Modelagem dos dados
- Correta utilização dos métodos e códigos HTTP
- Organização do código fonte
- Desenvolvimento de testes (unitários e/ou integração). Será um diferencial se você utilizar BDD.
- Autenticação
- Segurança das APIs
- Utilização de padrões de desenvolvimento

**Importante**: É melhor fazer pouco bem-feito do que tentar fazer muito e não fazer bem-feito, por isso, tudo que você implementar será considerado durante a avaliação. 
